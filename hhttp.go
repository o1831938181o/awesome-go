package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	//Package http provides HTTP client and server implementations.
)

func main1() {
	//parameter 1 : string pattern matching ; parameter 2: Handler
	http.Handle("/", http.FileServer(http.Dir(".")))
	http.ListenAndServe(":8080", nil)
}

func getBaidu() {

	url := "http://www.baidu.com"
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
		return
	}

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(body))
}
