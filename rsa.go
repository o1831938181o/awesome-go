package main

import (
	"crypto/rand"
	"crypto/rsa"
)

//https://studygolang.com/articles/22530
import (
	"crypto/x509"
	"encoding/pem"
)

// RSA公钥私钥产生
func GenRsaKey() (prvkey, pubkey []byte) {
	// 生成私钥文件
	privateKey, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		panic(err)
	}

	derStream := x509.MarshalPKCS1PrivateKey(privateKey)
	block := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: derStream,
	}
	prvkey = pem.EncodeToMemory(block)

	publicKey := privateKey.Public()
	derPkix, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		panic(err)
	}
	//first create a pem block
	block = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: derPkix,
	}
	pubkey = pem.EncodeToMemory(block)
	return
}

func generateKeys() (prvkey, pubKey []byte) {
	privateKey, _ := rsa.GenerateKey(rand.Reader, 1024)

	priBytes := x509.MarshalPKCS1PrivateKey(privateKey)
	block := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: priBytes,
	}
	prvkey = pem.EncodeToMemory(block)

	publicKey := &privateKey.PublicKey
	pubBytes := x509.MarshalPKCS1PublicKey(publicKey)
	//first create a pem block
	block = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: pubBytes,
	}
	pubKey = pem.EncodeToMemory(block)
	return
}
