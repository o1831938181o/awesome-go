package main

import (
	"fmt"
)

type Phone interface {
	call()
}

type NokiaPhone struct {
}

func (nokiaPhone NokiaPhone) call() {
	fmt.Println("I am Nokia, I can call you!")
}

type IPhone struct {
}

func (iPhone IPhone) call() {
	fmt.Println("I am iPhone, I can call you!")
}

func test() {
	var phone Phone

	phone = NokiaPhone{}
	phone.call()

	// builtin.go func

	// it's a interface
	phone = new(IPhone)
	phone.call()

}

func test1() {
	var i *NokiaPhone
	i = new(NokiaPhone)
	*i = NokiaPhone{}
	fmt.Println(*i)
}
