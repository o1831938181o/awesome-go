package main

import (
	"fmt"
	"math"
)

func max(num1, num2 int) int {
	var result int
	if num1 > num2 {
		result = num1
	} else {
		result = num2
	}
	return result
}

// 全局变量
// Go 语言函数作为实参
var getSquareRoot = func(x float64) float64 {
	return math.Sqrt(x)
}

// 函数返回多个值
func swap(x, y string) (string, string) {

	/* 使用函数 */
	fmt.Println(getSquareRoot(9))
	return y, x
}

// 函数参数

// 函数如果使用参数，该变量可称为函数的形参。

// 形参就像定义在函数体内的局部变量。

// 调用函数，可以通过两种方式来传递参数：

// 传递类型	描述
// 值传递	值传递是指在调用函数时将实际参数复制一份传递到函数中，这样在函数中如果对参数进行修改，将不会影响到实际参数。
// 引用传递	引用传递是指在调用函数时将实际参数的地址传递到函数中，那么在函数中对参数所进行的修改，将影响到实际参数。
// 默认情况下，Go 语言使用的是值传递，即在调用过程中不会影响到实际参数。

// https://www.runoob.com/go/go-functions.html
// https://www.runoob.com/go/go-function-closures.html

// 函数值(Function values): 函数也是值。他们可以像其他值一样传递，比如，函数值可以作为函数的参数或者返回值。
// https://cyent.github.io/golang/datatype/funcvalue_main/

func bb(x, y int) int {
	return x + y
}

func ccc() {
	dd := func(x, y int) int {
		return x - y
	}
	fmt.Printf("%T\n", bb)
	fmt.Printf("%T\n", dd)
	fmt.Println(dd1(dd))

}

func dd1(i func(int, int) int) int {
	fmt.Printf("i type: %T\n", i)
	return i(1, 2)
}

func adder() func(int) int {
	sum := 0
	return func(x int) int {
		sum += x
		return sum
	}
}

// 没有闭包的时候，函数就是一次性买卖，函数执行完毕后就无法再更改函数中变量的值（应该是内存释放了）；
// 有了闭包后函数就成为了一个变量的值，只要变量没被释放，函数就会一直处于存活并独享的状态，
// 因此可以后期更改函数中变量的值（因为这样就不会被go给回收内存了，会一直缓存在那里）。
func abc() func(int) int {
	res := 0
	return func(x int) int {
		res = (res + x) * 2
		return res
	}
}
