package main

import (
	"fmt"
)

func basicsyntex() {
	// 在 Go 程序中，一行代表一个语句结束。每个语句不需要像 C 家族中的其它语言一样以分号 ; 结尾，
	//因为这些工作都将由 Go 编译器自动完成。
	fmt.Println("Hello, World!")
	fmt.Println("菜鸟教程：runoob.com")

	//注释
	// 单行注释
	/**
	这是多行注释
	*/

	//标识符
	//标识符用来命名变量、类型等程序实体。
	//一个标识符实际上就是一个或是多个字母(A~Z和a~z)数字(0~9)、下划线_组成的序列，
	//但是第一个字符必须是字母或下划线而不能是数字。

	// 字符串连接
	fmt.Println("hello " + "world xxxxxxxx")

	// 变量定义
	var age int
	age = 22
	fmt.Println(age)

	//Go 语言中使用 fmt.Sprintf 格式化字符串并赋值给新串：
	// %d 表示整型数字，%s 表示字符串
	var stockcode = 123
	var enddate = "2020-12-31"
	var url = "Code=%d&endDate=%s"
	var target_url = fmt.Sprintf(url, stockcode, enddate)
	fmt.Println(target_url)

	// go 数据类型
	//https://www.runoob.com/go/go-data-types.html
	var a = "mother fucking"

	// 	var a *int
	// var a []int
	// var a map[string] int
	// var a chan int
	// var a func(string) int
	// var a error // error 是接口
	fmt.Println(a + "sfgd")

	// var b, c int = 1, 2
	// fmt.Println(b, c)

	var i int
	var f float64
	var b bool
	var s string
	fmt.Printf("%v %v %v %q\n", i, f, b, s)
	f1 := "Runoob" // equals == ==> var f string = "Runoob"
	fmt.Println(f1)

	// 多变量声明
	//类型相同多个变量, 非全局变量
	// var vname1, vname2, vname3 type
	// vname1, vname2, vname3 = v1, v2, v3

	// var vname1, vname2, vname3 = v1, v2, v3 // 和 python 很像,不需要显示声明类型，自动推断

	// vname1, vname2, vname3 := v1, v2, v3 // 出现在 := 左侧的变量不应该是已经被声明过的，否则会导致编译错误

	// // 这种因式分解关键字的写法一般用于声明全局变量
	// var (
	//     vname1 v_type1
	//     vname2 v_type2
	// )

}

var x, y int
var ( // 这种因式分解关键字的写法一般用于声明全局变量
	a int
	b bool
)

var c, d int = 1, 2
var e, f = 123, "hello"

// 这种不带声明格式的只能在函数体中出现
// g, h := 123, "hello"
// 简短形式，使用 := 赋值操作符
func variabledefine() {
	g, h := 123, "hello"
	println(x, y, a, b, c, d, e, f, g, h)
}

func testConstant() {
	const LENGTH int = 10
	const WIDTH int = 5
	var area int
	const a, b, c = 1, false, "str" //多重赋值

	area = LENGTH * WIDTH
	fmt.Printf("面积为 : %d", area)
	println()
	println(a, b, c)

	const (
		Unknown = 0
		Female  = 1
		Male    = 2
	)

	// iota
	// 	iota，特殊常量，可以认为是一个可以被编译器修改的常量。

	// iota 在 const关键字出现时将被重置为 0(const 内部的第一行之前)，const 中每新增一行常量声明将使 iota 计数一次(iota 可理解为 const 语句块中的行索引)。

	// iota 可以被用作枚举值：

}

func testConstIota() {
	const (
		a = iota //0
		b        //1
		c        //2
		d = "ha" //独立值，iota += 1
		e        //"ha"   iota += 1
		f = 100  //iota +=1
		g        //100  iota +=1
		h = iota //7,恢复计数
	)
	fmt.Println()
	fmt.Println(a, b, c, d, e, f, g, h)

	const (
		i = 1 << iota
		j = 3 << iota
		k
		l
	)

	fmt.Println("i=", i)
	fmt.Println("j=", j)
	fmt.Println("k=", k)
	fmt.Println("l=", l)
}

func testOperator() {
	// https://www.runoob.com/go/go-operators.html

	// 算术运算符
	// + - * / % ++ --

	// 关系运算符
	// == != > < >= <=

	// 逻辑运算符
	// && || !

	// 位运算符
	// & | ^ << >>

	// 赋值运算符
	// = += -= *= /= %= <<= >>= &= ^= |=

	// 其他运算符
	// & *

	// 运算符优先级

}

func testIfElse() {
	var a int = 10
	if a < 20 {
		fmt.Println("a less than 20")
	} else {
		fmt.Println("a great than 20")
	}
	fmt.Printf("a 的值为 : %d\n", a)
}

func ifififi() {
	var a int = 100
	var b int = 200
	if a == 100 {
		if b == 200 {
			fmt.Println("a is 100 and b is 200")
		}
	}
}

func switchswitch() {
	/* 定义局部变量 */
	var grade string = "B"
	var marks int = 90

	switch marks {
	case 90:
		grade = "A"
	case 80:
		grade = "B"
	case 50, 60, 70:
		grade = "C"
	default:
		grade = "D"
	}

	switch {
	case grade == "A":
		fmt.Printf("优秀!\n")
	case grade == "B", grade == "C":
		fmt.Printf("良好\n")
	case grade == "D":
		fmt.Printf("及格\n")
	case grade == "F":
		fmt.Printf("不及格\n")
	default:
		fmt.Printf("差\n")
	}
	fmt.Printf("你的等级是 %s\n", grade)
}

// 类型断言
// https://go.dev/tour/methods/16
// switch 语句还可以被用于 type-switch 来判断某个 interface 变量中实际存储的变量类型。
func switchtype() {
	var x interface{}

	switch i := x.(type) {
	case nil:
		fmt.Printf(" x 的类型 :%T", i)
	case int:
		fmt.Printf("x 是 int 型")
	case float64:
		fmt.Printf("x 是 float64 型")
	case func(int) float64:
		fmt.Printf("x 是 func(int) 型")
	case bool, string:
		fmt.Printf("x 是 bool 或 string 型")
	default:
		fmt.Printf("未知型")
	}
}

// https://www.runoob.com/go/go-select-statement.html
func switchselect() {

}

// Go 语言中 range
// 关键字用于 for 循环中迭代数组(array)、切片(slice)、通道(channel)或集合(map)的元素。

func forforfor() {
	// for init; condition; post { }
	// for condition { }
	// for { }

	// for key, value := range oldMap {
	// 	newMap[key] = value
	// }

	sum := 0
	for i := 0; i <= 10; i++ {
		sum += i
	}
	fmt.Println(sum)

	for sum <= 10 {
		sum += sum
	}
	fmt.Println(sum)

	// 这样写也可以，更像 While 语句形式
	for sum <= 10 {
		sum += sum
	}
	fmt.Println(sum)

	map1 := make(map[int]float32)
	map1[1] = 1.0
	map1[2] = 2.0
	map1[3] = 3.0
	map1[4] = 4.0

	// 读取 key 和 value
	for key, value := range map1 {
		fmt.Printf("key is: %d - value is: %f\n", key, value)
	}

	// 读取 key
	for key := range map1 {
		fmt.Printf("key is: %d\n", key)
	}

	// 读取 value
	for _, value := range map1 {
		fmt.Printf("value is: %f\n", value)
	}

	// for loop  with range keywords
	var pow = []int{1, 2, 4, 8, 16, 32, 64, 128}
	for i, v := range pow {
		fmt.Printf("2**%d = %d\n", i, v)
	}
}

// 不再纠结于 i++ 和 ++i
// 在Go语言中，自增操作符不再是一个操作符，而是一个语句
func plusplus() {
	var a, b int
	a++
	b++
	fmt.Println(a, b)
}

// func main() {
// 	basicsyntex()
// 	variabledefine()
// 	testConstant()
// 	testConstIota()
// 	testOperator()
// 	testIfElse()
// 	ifififi()
// 	switchswitch()
// 	forforfor()
// }
