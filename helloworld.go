package main

import "fmt"
import "rsc.io/quote"

func helloworld() {
	fmt.Println("hello world")
	//import the rsc.io/quote package and add a call to its Go function.
	fmt.Println(quote.Go())
}

func add(a int, b int) int {
	return a + b
}

func swap1(x, y string) (string, string) {
	return "", ""
}

func foo23(a, b int) (c int) {
	return a + b
}

// DECLARATION
// https://cyent.github.io/golang/basic/variable_declare/
//func main() {
//	fmt.Println("hello world")
//	fmt.Println(foo23(1, 2))
//}
