package main

import "fmt"

// 菜鸟

type MyString string

type Person struct {
	Name  string
	Age   int
	Email string
}

func (m Person) String() string {
	return fmt.Sprintf("MyString=%s", m.Name)
}

func (m MyString) String() string {
	return fmt.Sprintf("MyString=%s", string(m))
}

// https://blog.csdn.net/weixin_40165163/article/details/92422524
func main() {

	//iota
	//iota，特殊常量，可以认为是一个可以被编译器修改的常量。
	//
	//iota 在 const关键字出现时将被重置为 0(const 内部的第一行之前)，
	//const 中每新增一行常量声明将使 iota 计数一次(iota 可理解为 const 语句块中的行索引)。
	const (
		i = 5
		b
		j
		k
		l = 23 << iota
		m = iota
		n
	)

	const (
		a  = iota
		b1 = iota
		c  = iota
	)

	//for i := 0; i < 5; i++ {
	//	fun(i)
	//}
	//
	//prvKey, pubKey := generateKeys()
	//fmt.Println(string(prvKey))
	//fmt.Println(string(pubKey))

	//getBaidu()

	//a := MyString("hello world")
	//fmt.Print(a)

	p := &Person{
		Name:  "erfe",
		Age:   1,
		Email: "dfdsfaasf",
	}

	fmt.Print(p)
}

func fun(p int) {
	trace(p)
	defer untrace(p)
}

func untrace(p int) {
	fmt.Println(p, "enter")
}

func trace(p int) {
	fmt.Println(p, "leave")
}
