package main

import (
	"fmt"
	"io"
	"os"
	"reflect"
)

// usage of
// make
// delete
func mapmapmap() {
	mm := make(map[string]string)
	mm["France"] = "paris"
	mm["China"] = "BeiJing"
	for key := range mm {
		fmt.Printf("key is: %d\n", key)
	}

	delete(mm, "France")
	for key := range mm {
		fmt.Printf("key is: %d\n", key)
	}

	for key, value := range mm {
		fmt.Printf("key is: %d and value is: %d\n", key, value)
	}

}

// usage of
// len
func lenlenlen() {
	lena := len("aaaaa")
	lenb := len([]int{1, 2, 3, 4, 5})
	mm := make(map[string]string)
	mm["France"] = "paris"
	mm["China"] = "BeiJing"
	lenc := len(mm)
	fmt.Println("%d %d %d", lena, lenb, lenc)
}

// usage of
// cap
func capcapcap() {
	arr := []int{2, 3, 5, 7, 11, 13}

	sli := arr[1:4]
	fmt.Println(sli)
	fmt.Println(len(sli))
	fmt.Println(cap(sli))

	fmt.Println(sli[0:cap(sli)])
}

// usage of
// append
// copy
func sliceslice() {
	var numbers []int
	fmt.Println("type:", reflect.TypeOf(numbers))

	printSlice(numbers)

	/* 允许追加空切片 */
	numbers = append(numbers, 0)
	printSlice(numbers)

	/* 向切片添加一个元素 */
	numbers = append(numbers, 1)
	printSlice(numbers)

	/* 同时添加多个元素 */
	numbers = append(numbers, 2, 3, 4)
	printSlice(numbers)

	/* 创建切片 numbers1 是之前切片的两倍容量*/
	numbers1 := make([]int, len(numbers), (cap(numbers))*2)

	/* 拷贝 numbers 的内容到 numbers1 */
	copy(numbers1, numbers)
	printSlice(numbers1)

}

func printSlice(x []int) {
	fmt.Printf("len=%d cap=%d slice=%v\n", len(x), cap(x), x)
}

// usage of
// new
func newnewnew() {
	a := new(int)
	fmt.Println(a)
	*a = 1223
	fmt.Print(*a)
}

// defer 语句会将函数推迟到外层函数返回之后执行。
// 推迟调用的函数其参数会立即求值，但直到外层函数返回前该函数都不会被调用。
// 推迟的函数调用会被压入一个栈中。当外层函数返回时，被推迟的函数会按照后进先出的顺序调用。
func deferdefer() {
	defer fmt.Println("world")
	fmt.Println("hello")

	fmt.Println("counting")

	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}

	fmt.Println("done")
}

func CopyFile(dstName, srcName string) (写入 int64, err error) {
	src, err := os.Open(srcName)
	if err != nil {
		return
	}
	defer src.Close()

	dst, err := os.Create(dstName)
	if err != nil {
		return
	}
	defer dst.Close()

	return io.Copy(dst, src)
}

func ttt() {
	b := make([]int, 0, 5) // len(b)=0, cap(b)=5
	fmt.Println(b)
}

// func main() {
// 	mapmapmap()
// 	lenlenlen()
// 	newnewnew()
// 	capcapcap()

// 	sliceslice()
// }
