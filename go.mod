module awesomeProject

go 1.18

require (
	golang.org/x/sys v0.0.0-20220622161953-175b2fd9d664
	rsc.io/quote v1.5.2
)

require (
	golang.org/x/text v0.0.0-20170915032832-14c0d48ead0c // indirect
	rsc.io/sampler v1.3.0 // indirect
)
