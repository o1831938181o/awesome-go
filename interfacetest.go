package main

// "math"

// type geometry interface {
// 	area() float64
// 	perim() float64
// }

// type rect struct {
// 	width, height float64
// }

// type circle struct {
// 	radius float64
// }

// func (r rect) area() float64 {
// 	return r.width * r.height
// }

// func (r rect) perim() float64 {
// 	return 2*r.width + 2*r.height
// }

// func (c circle) area() float64 {
// 	return math.Pi * c.radius * c.radius
// }

// func (c circle) perim() float64 {
// 	return 2 * math.Pi * c.radius
// }

// func measure(g geometry) {
// 	fmt.Println(g)
// 	fmt.Println(g.area())
// 	fmt.Println(g.perim())
// }

// type Person struct {
// 	name string
// }

// func (p *Person) printName() {
// 	fmt.Println(p.name)
// }

// type SchoolMember struct {
// 	name string
// 	age  int
// }

// func (this SchoolMember) tell() {
// 	fmt.Printf("name:\"%s\", age:\"%d\"\n", this.name, this.age)
// }

// type Teacher struct {
// 	SchoolMember
// 	salary int
// }

// func (this Teacher) tell() {
// 	this.SchoolMember.tell()
// 	fmt.Printf("Salary: \"%d\"\n", this.salary)
// }

// type Student struct {
// 	SchoolMember
// 	marks int
// }

// func (this Student) tell() {
// 	this.SchoolMember.tell()
// 	fmt.Printf("Marks: \"%d\"\n", this.marks)
// }

// // https://cyent.github.io/golang/method/method_receiver/

// type MyStruct struct {
// 	x int
// }

// func (m MyStruct) Set1() {
// 	m.x = 1
// }

// func (m *MyStruct) Set2() {
// 	m.x = 2
// }

// // 因此，若没有涉及性能问题，且method的功能是读，而非写的时候，首选value receiver

// type Adder interface {
// 	add() int
// }

// type MyStruct1 struct {
// 	X, Y int
// }

// func (a *MyStruct1) add() int {
// 	return a.X + a.Y
// }

// type Adder1 interface {
// 	Add(int, int) (int, int)
// }

// type Foo interface {
// 	foo() int
// }

// type MyStruct struct {
// 	X, Y int
// }

// func (a *MyStruct) foo() int {
// 	return a.X + a.Y
// }

// func main() {

// 	s := MyStruct{3, 4}
// 	var f Foo = &s
// 	fmt.Printf("%v,%T\n", f, f)
// 	switch v := f.(type) {
// 	case *MyStruct:
// 		fmt.Printf("1,%v,%T\n", v, v)
// 	default:
// 		fmt.Printf("2,%v,%T\n", v, v)
// 	}

// 	// var i interface{} = 100

// 	// s, ok := i.(MyStruct1)
// 	// fmt.Println(s, ok)
// 	// fmt.Printf("%T\n", s)

// 	// f, ok := i.(float64)
// 	// fmt.Println(f, ok)
// 	// fmt.Printf("%T\n", f)

// 	// r := rect{width: 3, height: 4}
// 	// c := circle{radius: 5}
// 	// r.area()
// 	// measure(r)
// 	// measure(c)
// }

// type Foo interface {
// 	foo() int
// }

// type Adder interface {
// 	Add()
// }

// type MyStruct struct {
// 	X, Y int
// }

// // func (a *MyStruct) foo() int {
// // 	return a.X + a.Y
// // }

// func (this *MyStruct) Add() {
// 	fmt.Println(this.X + this.Y)
// }

// func main() {
// 	s := MyStruct{3, 4}
// 	// var f Foo = &s

// 	var i Adder = s
// }

// type Adder interface {
// 	add() int
// }

// type MyStruct struct {
// 	X, Y int
// }

// func (a *MyStruct) add() int {
// 	return a.X + a.Y
// }

// func main() {
// 	var f Adder
// 	s := MyStruct{3, 4}
// 	f = s
// 	fmt.Println(f.add())
// }

type Adder interface {
	Add(int, int) int
}

type MyStruct struct {
	X, Y int
}

func (m *MyStruct) Add(a, b int) int {
	return a + b
}

// func main() {
// 	var a Adder
// 	a = &MyStruct{3, 4}
// 	res := a.Add(5, 6)
// 	fmt.Println(res)
// }

// func main() {

// 	go say("world")
// 	say("hello")
// 	// pos := func() int { return 1 }
// 	// fmt.Printf("%v,%T\n", pos, pos)

// 	// var i interface{}
// 	// i = pos
// 	// fmt.Printf("%v,%T\n", i, i)

// 	// switch v := i.(type) {
// 	// case int:
// 	// 	fmt.Printf("1,%v,%T\n", v, v)
// 	// case func() int:
// 	// 	fmt.Printf("2,%v,%T\n", v, v)
// 	// case func(int) int:
// 	// 	fmt.Printf("3,%v,%T\n", v, v)
// 	// default:
// 	// 	fmt.Printf("4,%v,%T\n", v, v)
// 	// }

// }
