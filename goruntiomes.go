package main

import (
	"fmt"
	"golang.org/x/sys/unix"
	"time"
)

func foo(i int, c chan int) {
	c <- i
	fmt.Println("send:", i)
}

// func main() {
// 	c := make(chan int)
// 	go foo(0, c)
// 	res := <-c
// 	fmt.Println("receive:", res)
// }

// type Reader interface {
// 	Read() int
// }

// type MyStruct1 struct {
// 	X Reader
// 	Y int
// }

// type Foo struct {
// 	a, b int
// }

// func (f *Foo) Read() int {
// 	return f.a + f.b
// }

// func main() {
// 	ms := &MyStruct1{}
// 	fmt.Printf("%T %v\n", ms, ms)

// 	ms.Y = 3
// 	fmt.Printf("%T %v\n", ms, ms)

// 	ms.X = &Foo{8, 9}
// 	fmt.Printf("%T %v\n", ms, ms)
// 	fmt.Printf("%T %v\n", ms.X, ms.X)
// 	fmt.Printf("%T %v\n", ms.X.Read(), ms.X.Read())
// }

//https://blog.csdn.net/ce123_zhouwei/article/details/118633387

func see(s string) {
	//runtime.LockOSThread()

	a := time.Now()
	for i := 1; i < 20000; i++ {
		//runtime.Gosched()
		var sum int
		sum = 1
		sum = sum * i
		fmt.Println("sum:", sum)
		fmt.Println("s:", s)
		getid("see " + s)
		//fmt.Println("cpus:", runtime.StackRecord{})
		//fmt.Println("cpus:", runtime.LockOSThread)
	}
	fmt.Println(time.Since(a))
}
func getid(s string) {
	//pid := unix.Getpid()
	//fmt.Println(s+" Getpid", pid)
	//fmt.Println(s+" Getppid", unix.Getppid())

	//pgid, _ := unix.Getpgid(pid)
	//fmt.Println(s+" Getpgid", pgid)
	//
	fmt.Println(s+" Gettid", unix.Gettid())
	//
	//sid, _ := unix.Getsid(pid)
	//fmt.Println(s+" Getsid", sid)
	//
	//fmt.Println(s+" Getegid", unix.Getegid())
	//fmt.Println(s+" Geteuid", unix.Geteuid())
	//fmt.Println(s+" Getgid", unix.Getgid())
	//fmt.Println(s+" Getuid", unix.Getuid())
}

// 这里是 worker，我们将并发执行多个 worker。
// worker 将从 `jobs` 通道接收任务，并且通过 `results` 发送对应的结果。
// 我们将让每个任务间隔 1s 来模仿一个耗时的任务。
func worker(id int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		getid("ss")
		fmt.Println("worker", id, "processing job", j)
		time.Sleep(time.Second)
		getid("ss")
		results <- j * 2
	}
}

//func main() {
//
//	// 为了使用 worker 线程池并且收集他们的结果，我们需要 2 个通道。
//	jobs := make(chan int, 100)
//	results := make(chan int, 100)
//
//	// 这里启动了 3 个 worker，初始是阻塞的，因为还没有传递任务。
//	for w := 1; w <= 3; w++ {
//		go worker(w, jobs, results)
//	}
//
//	// 这里我们发送 9 个 `jobs`，然后 `close` 这些通道
//	// 来表示这些就是所有的任务了。
//	for j := 1; j <= 9; j++ {
//		jobs <- j
//	}
//	close(jobs)
//
//	// 最后，我们收集所有这些任务的返回值。
//	for a := 1; a <= 9; a++ {
//		<-results
//	}
//}
