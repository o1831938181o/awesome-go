package main

import "fmt"

// https://www.runoob.com/go/go-slice.html
// https://www.runoob.com/go/go-range.html
// https://www.runoob.com/go/go-map.html

// https://www.runoob.com/go/go-type-casting.html
func tyeee() {
	var sum int = 17
	var count int = 5
	var mean float32

	mean = float32(sum) / float32(count)
	fmt.Printf("mean 的值为: %f\n", mean)
}
