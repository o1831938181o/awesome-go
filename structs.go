package main

import "fmt"

type Books struct {
	title   string
	author  string
	subject string
	book_id int
}
type handle func(str string) //自定义一个函数func，别名叫做handle，传入一个string参数

func usage() {
	fmt.Println(Books{"Go lang", "www.peter.com", "Go lang", 666})
	fmt.Println(Books{title: "Go 语言", author: "www.runoob.com", subject: "Go 语言教程", book_id: 6495407})
	fmt.Println(Books{title: "Go 语言", author: "www.runoob.com"})

}

// 结构体指针
// var struct_pointer *Books
// struct_pointer = &Book1
// 使用结构体指针访问结构体成员，使用 "." 操作符：
// struct_pointer.title
